# passport-depositphotos

[![build status](https://gitlab.com/depositphotos/passport-depositphotos/badges/master/build.svg)](https://gitlab.com/depositphotos/passport-depositphotos/commits/master)
[![coverage report](https://gitlab.com/depositphotos/passport-depositphotos/badges/master/coverage.svg)](https://gitlab.com/depositphotos/passport-depositphotos/commits/master)

An abstract class implementing [Passport](http://passportjs.org/)'s strategy API.

## Install

    $ npm install passport-depositphotos

## Usage

This module use an abstract `Strategy` class that is intended to be
subclassed when implementing concrete authentication strategies.  Once
implemented, such strategies can be used by applications that utilize Passport
middleware for authentication.

#### Configure Strategy

The Depositphotos authentication strategy authenticates users using a Depositphotos account.
The strategy requires a `verify` callback, which accepts
these credentials and calls `done` providing a user, as well as `options`
specifying a API key.

    passport.use(new DepositphotosStrategy({
        apiKey: process.env.DEPOSITPHOTOS_API_KEY,
      }, function(accessToken, profile, done) {
        User.findOrCreate({ depositphotosId: profile.id }, function (err, user) {
          return done(err, user);
        });
      }
    ));

#### Authenticate Requests

Use `passport.authenticate()`, specifying the `'depositphotos'` strategy, to
authenticate requests.

For example, as route middleware in an [Express](http://expressjs.com/)
application:

    app.get('/auth/depositphotos',
      passport.authenticate('depositphotos'));

    app.get('/auth/depositphotos/callback',
      passport.authenticate('depositphotos', { failureRedirect: '/login' }),
      function(req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
      });

## Tests

    $ npm install
    $ npm test

## Credits

  - [Oleksii Volkov](https://gitlab.com/alexey-wild)

## License

[The MIT License](http://opensource.org/licenses/MIT)

Copyright (c) 1988-2017 Oleksii Volkov <[https://ovolkov.pro/](https://ovolkov.pro/)>
