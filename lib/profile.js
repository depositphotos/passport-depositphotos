'use strict';

const debug = require('debug')('passport:depositphotos:profile');

/**
 * Parse profile.
 *
 * @param {String} value
 *
 * @return {String}
 *
 * @api private
 */
function validateString(value) {
  if (!value) {
    return '';
  }

  value = String(value);
  return value.trim();
}

/**
 * Parse profile.
 *
 * @param {String}        userId
 * @param {Object|String} json
 *
 * @return {Object}
 *
 * @api public
 */
exports.parse = function(userId, json) {
  debug('Parsing user profile', json);

  if ('string' === typeof json) {
    json = JSON.parse(json);
  }

  if (json.user) {
    json = json.user;
  }

  let profile = {};
  profile.id = validateString(userId);
  profile.displayName = validateString(json.username);
  profile.name = profile.name || {};
  profile.name.givenName = validateString(json.firstName);
  profile.name.familyName = validateString(json.lastName);
  profile.username = validateString(json.username);
  profile.language = validateString(json.language) || 'en';
  profile.profileUrl = validateString(json.avatar);
  if (json.email) {
    profile.emails = [{
      value: validateString(json.email)
    }];
  }

  return profile;
};