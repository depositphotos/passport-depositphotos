'use strict';

const debug = require('debug')('passport:depositphotos:strategy');
const util = require('util');
const querystring = require('querystring');
const fetch = require('node-fetch');
const passport = require('passport-strategy');

const lookup = require('./utils').lookup;
const Profile = require('./profile');

const BASE_URL = 'https://api.depositphotos.com';

/**
 * `Strategy` constructor.
 *
 * The depositphotos authentication strategy authenticates requests based on the
 * credentials submitted through an HTML-based login form.
 *
 * Applications must supply a `verify` callback which accepts `username` and
 * `password` credentials, and then calls the `done` callback supplying a
 * `user`, which should be set to `false` if the credentials are not valid.
 * If an exception occurred, `err` should be set.
 *
 * Optionally, `options` can be used to change the fields in which the
 * credentials are found.
 *
 * Options:
 *   - `usernameField`  field name where the username is found, defaults to _username_
 *   - `passwordField`  field name where the password is found, defaults to _password_
 *   - `passReqToCallback`  when `true`, `req` is the first argument to the verify callback (default: `false`)
 *
 * Examples:
 *
 *     passport.use(new DepositphotosStrategy(
 *       function(username, password, done) {
 *         User.findOne({ username: username, password: password }, function (err, user) {
 *           done(err, user);
 *         });
 *       }
 *     ));
 *
 * @param {Object} options
 * @param {Function} verify
 * @api public
 */
function Strategy(options, verify) {
  if (typeof options === 'function') {
    verify = options;
    options = {};
  }

  if (!options.apiKey) {
    throw new TypeError('DepositphotosStrategy requires a apiKey');
  }

  if (!verify) {
    throw new TypeError('DepositphotosStrategy requires a verify callback');
  }

  this.apiKey = options.apiKey;
  this._usernameField = options.usernameField || 'username';
  this._passwordField = options.passwordField || 'password';

  passport.Strategy.call(this);
  this.name = 'depositphotos';
  this._verify = verify;
  this._passReqToCallback = options.passReqToCallback;
}

/**
 * Inherit from `passport.Strategy`.
 */
util.inherits(Strategy, passport.Strategy);

/**
 * Authenticate request based on the contents of a form submission.
 *
 * @param {Object} req
 * @param {Object} options
 *
 * @api protected
 */
Strategy.prototype.authenticate = function(req, options = {}) {
  debug('Authenticate user');

  let username = lookup(req.body, this._usernameField) || lookup(req.query, this._usernameField);
  let password = lookup(req.body, this._passwordField) || lookup(req.query, this._passwordField);

  if (!username || !password) {
    return this.fail({ message: options.badRequestMessage || 'Missing credentials' }, 400);
  }

  let self = this;

  let verified = (err, user, info) => {
    if (err) {
      debug(err);
      return self.error(err);
    }

    if (!user) {
      debug(info);
      return self.fail(info, 400);
    }

    self.success(user, info);
  };

  debug('loginAsUser');
  fetch(`${BASE_URL}/?${querystring.stringify({
    'dp_apikey': self.apiKey,
    'dp_command': 'loginAsUser',
    'dp_login_user': username,
    'dp_login_password': password
  })}`)
    .then(res => res.json())
    .then((res) => {
      if ('success' !== res.type) {
        debug(res.error);
        return self.error(res.error);
      }

      self.sessionId = res.sessionid;
      self.userId = +res.userid;

      debug('getAuthToken');
      return fetch(`${BASE_URL}/?${querystring.stringify({
        'dp_apikey': self.apiKey,
        'dp_command': 'getAuthToken',
        'dp_session_id': self.sessionId,
        'dp_expire': 8760 // 1 year
      })}`);
    })
    .then(res => res.json())
    .then((res) => {
      if ('success' !== res.type) {
        debug(res.error);
        return self.error(res.error);
      }

      self.token = res.token;
      debug('getUserData');
      return fetch(`${BASE_URL}/?${querystring.stringify({
        'dp_apikey': self.apiKey,
        'dp_command': 'getUserData',
        'dp_session_id': self.sessionId,
        'dp_user_id': self.userId,
        'dp_datetime_format': 'unix',
      })}`);
    })
    .then(res => res.json())
    .then((res) => {
      if ('success' !== res.type) {
        debug(res.error);
        return self.error(res.error);
      }

      let profile = Profile.parse(self.userId, res);
      profile.provider = 'depositphotos';
      profile._raw = res;
      profile._json = res;

      if (self._passReqToCallback) {
        return self._verify(req, self.token, profile, verified);
      } else {
        return self._verify(self.token, profile, verified);
      }
    })
    .catch((err) => {
      debug(err);
      return self.error(err);
    });
};

/**
 * Expose `Strategy`.
 */
module.exports = Strategy;